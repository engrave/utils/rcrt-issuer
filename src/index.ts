import {Client, PrivateKey, Transaction} from '@hiveio/dhive'
import Axios from 'axios';
import assert from 'assert';
import * as _ from 'lodash';
import {HiveEngineAccountStats} from './HiveEngineAccountStats';
import {AccountToIssueTokens} from './AccountToIssueTokens';

const wait = (ms: number) => {
    console.log(`Waiting ${ms/1000} seconds`)
    return new Promise((resolve) => setTimeout(resolve, ms))
}

assert(process.env.ACTIVE_KEY, 'Please provide Active key as a ACTIVE_KEY environment variable');

const activeKey = PrivateKey.from(process.env.ACTIVE_KEY as string);
const client = new Client(['https://api.hive.blog', 'https://api.openhive.network']);

const getStakeRichList = async (): Promise<HiveEngineAccountStats[]> => {
    const {data} = await Axios.post('https://api.hive-engine.com/rpc/contracts', {
        jsonrpc: "2.0",
        id: 1,
        method: "find",
        params: {
            contract: "tokens",
            table: "balances",
            query: {
                symbol: "RCRT"
            },
            offset: 0,
            limit: 100
        }
    })
    // select accounts with staked tokens only
    return (<any>data).result.filter((holder: HiveEngineAccountStats) => (parseFloat(holder.stake) > 0));
}

const generateIssueTokensTransaction = async (accounts: AccountToIssueTokens[]): Promise<Transaction> => {
    const properties = await client.database.getDynamicGlobalProperties();
    return {
        ref_block_num: properties.head_block_number,
        ref_block_prefix: Buffer.from(properties.head_block_id, 'hex').readUInt32LE(4),
        expiration: new Date(Date.now() + 60 * 1000).toISOString().slice(0, -5),
        operations: accounts.map(account => ([
            "custom_json", {
                required_auths: ["rcrt"],
                required_posting_auths: [],
                id: "ssc-mainnet-hive",
                json: JSON.stringify({
                        contractName: "tokens",
                        contractAction: "issue",
                        contractPayload: {
                            symbol: "RCRT",
                            to: account.account,
                            quantity: account.tokensToIssue
                        }
                    }
                )
            }])),
        extensions: []
    }
}

const issueTokens = async () => {
    try {
        const holders = await getStakeRichList();
        console.log('Richlist for RCRT token:');
        console.table(holders);

        const accountsToIssueTokensFor = holders.map(holder => ({
            account: holder.account,
            stake: parseFloat(holder.stake).toFixed(3),
            tokensToIssue: ((parseFloat(holder.stake) * 2.5) / 100).toFixed(3)
        }))

        console.log('Will issue following tokens:')
        console.table(accountsToIssueTokensFor);

        // group hodlers by 5 to decrease transactions number
        for (const chunk of _.chunk(accountsToIssueTokensFor, 5)) {
            const tx = await generateIssueTokensTransaction(chunk);
            const signedTx = await client.broadcast.sign(tx, activeKey);

            // NOTE
            // comment this if you just want to test the script without broadcasting the transactions
            const {id} = await client.broadcast.send(signedTx);
            console.log(`Issued in tx ${id}. Click: https://hiveblocks.com/tx/${id}`);
            await wait(5000); // wait 5 seconds
        }
        console.log('All tokens issued correctly!')

    } catch (error) {
        console.log(error);
    }
}

issueTokens().then(() => console.log('Finished')).catch(console.error);