export interface HiveEngineAccountStats {
    account: string;
    balance: string;
    delegationsIn: string;
    delegationsOut: string;
    pendingUndelegations: string;
    pendingUnstake: string;
    stake: string;
    symbol: string;
    _id: number;
}
