export interface AccountToIssueTokens {
    account: string;
    tokensToIssue: string;
    stake: string;
}
